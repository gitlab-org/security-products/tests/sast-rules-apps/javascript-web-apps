import express from "express";
import cors from "cors";
import {expressjwt} from 'express-jwt';
import dotEnv from "dotenv";

dotEnv.config()

const PORT = 3000;

function createAndStartServer(routers) {
    const app = express();
    assignDefaultMiddleware(app);
    if (routers && Object.keys(routers).length > 0) {
      assignRouter(app, routers);
    }
  
    const server = app.listen(PORT, () => {
        console.log(`server running on port ${PORT}`);
      });
  }

function assignDefaultMiddleware(app) {
    app.use(express.json({ limit: "200mb" }));

    app.set('views',  './src/views')
    app.set('view engine', 'ejs')

    app.use(
        express.urlencoded({
            limit: "200mb",
            extended: true,
            parameterLimit: 1000000
        })
    )

    app.use(cors());
}

function assignRouter(app, routers) {
    Object.keys(routers).forEach((key) => app.use(key, routers[key]));
}


export default createAndStartServer;