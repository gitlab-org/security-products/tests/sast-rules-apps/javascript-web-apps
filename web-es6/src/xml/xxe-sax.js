import sax from "sax";
import express from "express";
import fs from 'fs';

const router = express.Router();

// http://localhost:3000/xml/sax/1
router.route("/1").get(async (req, res) => {
    var entityName, systemId

    var saxStream = sax.createStream(false)
    
    // ok: rules_lgpl_javascript_xml_rule-xxe-sax
    saxStream.on("error", function (e) {
        console.error("error!", e)
    })

    // ok: rules_lgpl_javascript_xml_rule-xxe-sax
    saxStream.onopentag = function (node) {
        console.log("opentag")
    }

    // ruleid: rules_lgpl_javascript_xml_rule-xxe-sax
    saxStream.ondoctype = async function (doctype) {
        const match = /<!ENTITY\s+(\w+)\s+SYSTEM\s+"([^"]+)"/.exec(doctype);
        if (match) {
            entityName = match[1];
            systemId = match[2];
            console.log(`Found external entity: ${entityName} with SYSTEM "${systemId}"`);
        }
    }

    fs.createReadStream("assets/xml/xxe_xml.xml")
        .pipe(saxStream)
        .pipe(fs.createWriteStream("assets/xml/xxe_xml_cpy.xml"))

    res.send(`Check server console`);
});

// http://localhost:3000/xml/sax/2
router.route("/2").get(async (req, res) => {
    var entityName, systemId

    var saxStream = sax.createStream(false)
    
    // ok: rules_lgpl_javascript_xml_rule-xxe-sax
    saxStream.on("error", function (e) {
        console.error("error!", e)
    })

    // ok: rules_lgpl_javascript_xml_rule-xxe-sax
    saxStream.on("opentag", function (node) {
        console.log("opentag")
    })

    // ruleid: rules_lgpl_javascript_xml_rule-xxe-sax
    saxStream.on("doctype", function (doctype) {
        const match = /<!ENTITY\s+(\w+)\s+SYSTEM\s+"([^"]+)"/.exec(doctype);
        if (match) {
            entityName = match[1];
            systemId = match[2];
            console.log(`Found external entity: ${entityName} with SYSTEM "${systemId}"`);
        }
    })

    fs.createReadStream("assets/xml/xxe_xml.xml")
        .pipe(saxStream)
        .pipe(fs.createWriteStream("assets/xml/xxe_xml_cpy.xml"))

    res.send(`Check server console`);
});


export default router;