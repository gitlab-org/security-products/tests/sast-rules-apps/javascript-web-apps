import express from "express";
import jwt from "jsonwebtoken"
import * as jose from 'jose'

const router = express.Router();

// http://localhost:3000/jwt/hardcoded-jwt-secret/jsonwebtoken/1
router.route("/jsonwebtoken/1").get((req, res) => {
    const payload = { user_id: 123, username: 'john_doe' };
    const secretKey = 'This is a secret key';

    // ruleid: rules_lgpl_javascript_jwt_rule-node-jwt-none-algorithm
    const token = jwt.sign(payload, secretKey);
    console.log('Generated Token:', token);
    res.send(token)
})

// http://localhost:3000/jwt/hardcoded-jwt-secret/jsonwebtoken/2
router.route("/jsonwebtoken/2").get((req, res) => {
    const secretKey = 'This is a secret key';
    const token = req.query.token;
    console.log('Generated Token:', token);

    // ruleid: rules_lgpl_javascript_jwt_rule-node-jwt-none-algorithm
    jwt.verify(token, secretKey, (err, decoded) => {
        if (err) {
            console.error('Token verification failed:', err.message);
        } else {
            console.log('Token verified successfully:', decoded);
        }
    });

    res.send("Check server console")
})

// http://localhost:3000/jwt/hardcoded-jwt-secret/jsonwebtoken/3
router.route("/jsonwebtoken/3").get((req, res) => {
    const payload = { user_id: 123, username: 'john_doe' };

    // ruleid: rules_lgpl_javascript_jwt_rule-node-jwt-none-algorithm
    const token = jwt.sign(payload, 'This is a secret key', { algorithm: 'HS256' });
    console.log('Generated Token:', token);

    // ruleid: rules_lgpl_javascript_jwt_rule-node-jwt-none-algorithm
    jwt.verify(token, 'This is a secret key', (err, decoded) => {
        if (err) {
            console.error('Token verification failed:', err.message);
        } else {
            console.log('Token verified successfully:', decoded);
        }
    });

    res.send("Check server console")
})

// http://localhost:3000/jwt/hardcoded-jwt-secret/jose/1
router.route("/jose/1").get(async (req, res) => {
    const payload = { user_id: 123, username: 'john_doe' };
    const key = new TextEncoder().encode(
        'cc7e0d44fd473002f1c42167459001140ec6389b7353f8088f4d9a95f2f596f2',
    )

    // ruleid: rules_lgpl_javascript_jwt_rule-node-jwt-none-algorithm
    const token = await new jose.SignJWT(payload)
        .setProtectedHeader({ alg: 'HS256' })
        .setIssuedAt()
        .setIssuer('urn:example:issuer')
        .setAudience('urn:example:audience')
        .setExpirationTime('2h')
        .sign(key)

    console.log('Generated Token: ', token);

    // ruleid: rules_lgpl_javascript_jwt_rule-node-jwt-none-algorithm
    const tokenDecrypted = await jose.jwtVerify(token, key, { algorithms: ['HS256'] }, {
        issuer: 'urn:example:issuer',
        audience: 'urn:example:audience',
    });

    console.log({ tokenDecrypted })
    res.send("Token: " + tokenDecrypted)
})

// http://localhost:3000/jwt/hardcoded-jwt-secret/jose/2
router.route("/jose/2").get(async (req, res) => {
    const payload = { user_id: 123, username: 'john_doe' };

    // ruleid: rules_lgpl_javascript_jwt_rule-node-jwt-none-algorithm
    const token = await new jose.SignJWT(payload)
        .setProtectedHeader({ alg: 'HS256' })
        .setIssuedAt()
        .setIssuer('urn:example:issuer')
        .setAudience('urn:example:audience')
        .setExpirationTime('2h')
        .sign(new TextEncoder().encode(
            'cc7e0d44fd473002f1c42167459001140ec6389b7353f8088f4d9a95f2f596f2',
        ))

    console.log('Generated Token: ', token);

    // ruleid: rules_lgpl_javascript_jwt_rule-node-jwt-none-algorithm
    const tokenDecrypted = await jose.jwtVerify(token,new TextEncoder().encode(
        'cc7e0d44fd473002f1c42167459001140ec6389b7353f8088f4d9a95f2f596f2',
    ), { algorithms: ['HS256'] }, {
        issuer: 'urn:example:issuer',
        audience: 'urn:example:audience',
    });

    console.log({ tokenDecrypted })
    res.send("Token: " + tokenDecrypted)
})

export default router;