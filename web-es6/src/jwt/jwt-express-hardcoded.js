import express from "express";
import { expressjwt } from 'express-jwt';
import jwt from "jsonwebtoken"

const router = express.Router();

const secretKey = 'yourSecretKey';

// NOTE: 
// Use http://localhost:3000/jwt/express-hardcoded/sign-in for token generation
// and use it in the rest of the test cases 
// SAMPLE CURL: 
// curl --location 'http://localhost:3000/jwt/express-hardcoded/safe-route' \
// --header 'Authorization: Bearer <TOKEN>'

// http://localhost:3000/jwt/express-hardcoded/sign-in
router.route("/sign-in").get((req, res) => {
    const payload = { user_id: 123, username: 'john_doe' };

    const token = jwt.sign(payload, secretKey,{ algorithm: 'HS256', issuer: 'http://issuer' });
    console.log('Generated Token:', token);
    res.send(token)

});

// http://localhost:3000/jwt/express-hardcoded/auth-route-1
router.route("/auth-route-1").get(
    // ruleid: rules_lgpl_javascript_jwt_rule-jwt-express-hardcoded
    expressjwt({ secret: 'yourSecretKey', algorithms: ['HS256'] }),
    (req, res) => {
        res.send("auth-route-auth-route-1 response");
    }
);

// http://localhost:3000/jwt/express-hardcoded/auth-route-2
router.route("/auth-route-2").get(
    // ruleid: rules_lgpl_javascript_jwt_rule-jwt-express-hardcoded
    expressjwt({ secret: secretKey, algorithms: ['HS256'] }),
    (req, res) => {
        res.send("auth-route-auth-route-2 response");
    }
);

const options = { secret: secretKey, algorithms: ['HS256'] }
// http://localhost:3000/jwt/express-hardcoded/auth-route-3
router.route("/auth-route-3").get(
    // ruleid: rules_lgpl_javascript_jwt_rule-jwt-express-hardcoded
    expressjwt(options),
    (req, res) => {
        res.send("auth-route-auth-route-3 response");
    }
);

let secret2 = { secret: "yourSecretKey" }
const opts2 = Object.assign({ bar: "foo"  }, secret2, { algorithms: ['HS256'] })
// http://localhost:3000/jwt/express-hardcoded/auth-route-4
router.route("/auth-route-4").get(
    // ruleid: rules_lgpl_javascript_jwt_rule-jwt-express-hardcoded
    expressjwt(opts2),
    (req, res) => {
        res.send("auth-route-auth-route-4 response");
    }
);

// http://localhost:3000/jwt/express-hardcoded/safe-route
router.route("/safe-route").get(
    // ok: rules_lgpl_javascript_jwt_rule-jwt-express-hardcoded
    expressjwt({ secret: process.env.SECRET, algorithms: ['HS256'] }),
    (req, res) => {
        res.send("auth-route-safe-route response");
    }
);

export default router;