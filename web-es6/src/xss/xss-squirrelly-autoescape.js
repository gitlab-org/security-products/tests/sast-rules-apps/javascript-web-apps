import express from "express";
import Sqrl from 'squirrelly';

const router = express.Router();

// http://localhost:3000/xss/squirrelly/1?message=<script>alert('XSS Attack')</script>
router.route("/1").get((req, res) => {
    var myTemplate = "Alert shown using req.query.message {{message}}</p>"
    // ruleid: rules_lgpl_javascript_xss_rule-squirrelly-autoescape
    Sqrl.autoEscaping(false)
    res.send(Sqrl.Render(myTemplate, {message: req.query.message})) 
})

// http://localhost:3000/xss/squirrelly/safe/1?message=<script>alert('XSS Attack')</script>
router.route("/safe/1").get((req, res) => {
    var myTemplate = "<p>My Message is: {{message}}</p>"
    // ok: rules_lgpl_javascript_xss_rule-squirrelly-autoescape
    Sqrl.autoEscaping(true)
    res.send(Sqrl.Render(myTemplate, {message: req.query.message})) 
})

// http://localhost:3000/xss/squirrelly/safe/2?message=<script>alert('XSS Attack')</script>
router.route("/safe/2").get((req, res) => {
    var myTemplate = "<p>My Message is: {{message}}</p>"
    // ok: rules_lgpl_javascript_xss_rule-squirrelly-autoescape
    res.send(Sqrl.Render(myTemplate, {message: req.query.message})) 
})

export default router;