import serialize from "serialize-javascript";
import express from "express";

const router = express.Router();

// http://localhost:3000/xss/serialize/1?message=<script>alert('sd')</script>
router.route("/1").get(async (req, res) => {
    const htmlResponse = req.query.message + "The alert was shown from the query param";
    // ruleid: rules_lgpl_javascript_xss_rule-xss-serialize-javascript
    const jsObj = serialize({
        foo: htmlResponse
    }, { unsafe: true });
    res.send(jsObj);
});

// http://localhost:3000/xss/serialize/safe/1?message=<script>alert('sd')</script>
router.route("/safe/1").get(async (req, res) => {
    const htmlResponse = req.query.message + "The alert was shown from the query param";
    // ok: rules_lgpl_javascript_xss_rule-xss-serialize-javascript
    const jsObj = serialize({
        foo: htmlResponse
    });
    res.send(jsObj);
});

// http://localhost:3000/xss/serialize/safe/2?message=<script>alert('sd')</script>
router.route("/safe/2").get(async (req, res) => {
    const htmlResponse = req.query.message + "The alert was shown from the query param";
    // ok: rules_lgpl_javascript_xss_rule-xss-serialize-javascript
    const jsObj = serialize({
        foo: htmlResponse
    }, { unsafe: false });
    res.send(jsObj);
});

export default router;