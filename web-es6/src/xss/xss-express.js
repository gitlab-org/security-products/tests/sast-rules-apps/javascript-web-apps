import express from "express";

const router = express.Router();

 // http://localhost:3000/xss/express/0?name=%3Cscript%3Ealert(%27Hi%27)%3C/script%3E
router.get('/0', (req, res) => {
    const { name } = req.query;
    // ruleid: rules_lgpl_javascript_xss_rule-express-xss
    res.send('<h1> Hello :' + name + "</h1>")
})

 // http://localhost:3000/xss/express/safe/0?name=%3Cscript%3Ealert(%27Hi%27)%3C/script%3E
router.get('/safe/0', (req, res) => {
    var name = req.query.name
    // ok: rules_lgpl_javascript_xss_rule-express-xss
    res.render('index', { title: name });
})

 // http://localhost:3000/xss/express/safe/1?name=%3Cscript%3Ealert(%27Hi%27)%3C/script%3E
router.get('/safe/1', (req, res) => {
    var name = encodeURI(req.query.name);
    // ok: rules_lgpl_javascript_xss_rule-express-xss
    res.send('index', { title: name });
})

export default router;