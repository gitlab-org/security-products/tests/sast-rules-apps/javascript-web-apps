import express from "express";
import HandleBars from "handlebars";

const router = express.Router();

// http://localhost:3000/xss/handlebars/noescape/1?message=<script>alert('XSS')</script>
router.route("/1").get(async (req, res) => {
    var template = "This is {{target}}";
    // ruleid: rules_lgpl_javascript_xss_rule-handlebars-noescape
    var out = HandleBars.compile(template, { noEscape: true })({ target: req.query.message });
    res.send(out);
});

// http://localhost:3000/xss/handlebars/noescape/1?message=<script>alert('XSS')</script>
router.route("/2").get(async (req, res) => {
    var template = "This is {{target}}";
    // ruleid: rules_lgpl_javascript_xss_rule-handlebars-noescape
    var outFn = HandleBars.compile(template, { noEscape: true });
    var out = outFn({ target: req.query.message })
    res.send(out);
});

// http://localhost:3000/xss/handlebars/noescape/3?message=<script>alert('XSS')</script>
router.route("/3").get(async (req, res) => {
    var template = "This is {{target}}";
    var trueVar = true;
    // ruleid: rules_lgpl_javascript_xss_rule-handlebars-noescape
    var outFn = HandleBars.compile(template, { noEscape: trueVar });
    var out = outFn({ target: req.query.message })
    res.send(out);
});

// http://localhost:3000/xss/handlebars/noescape/safe/1?message=<script>alert('XSS')</script>
router.route("/safe/1").get(async (req, res) => {
    var template = "This is {{target}}";
    // ok: rules_lgpl_javascript_xss_rule-handlebars-noescape
    var out = HandleBars.compile(template)({ target: req.query.message });
    res.send(out);
});

// http://localhost:3000/xss/handlebars/noescape/safe/2?message=<script>alert('sd')</script>
router.route("/safe/2").get(async (req, res) => {
    var template = "This is {{target}}";
    // ok: rules_lgpl_javascript_xss_rule-handlebars-noescape
    var out = HandleBars.compile(template, { noEscape: false })({ target: req.query.message });
    res.send(out);
});

// http://localhost:3000/xss/handlebars/noescape/safe/3?message=<script>alert('sd')</script>
router.route("/safe/3").get(async (req, res) => {
    var template = "This is {{target}}";
    var target = "target";
    // ok: rules_lgpl_javascript_xss_rule-handlebars-noescape
    var out = HandleBars.compile(template, { noEscape: false })({ target: target });
    res.send(out);
});

// http://localhost:3000/xss/handlebars/noescape/safe/4?message=<script>alert('sd')</script>
router.route("/safe/4").get(async (req, res) => {
    var template = "This is {{target}}";
    var falseVar = false
    // ok: rules_lgpl_javascript_xss_rule-handlebars-noescape
    var out = HandleBars.compile(template, { noEscape: falseVar })({ target: req.query.message });
    res.send(out);
});

// http://localhost:3000/xss/handlebars/noescape/safe/5?message=<script>alert('sd')</script>
router.route("/safe/5").get(async (req, res) => {
    var template = "This is {{target}}";
    var falseVar = false
    // ok: rules_lgpl_javascript_xss_rule-handlebars-noescape
    var out = HandleBars.compile(template, { noEscape: true })({ target: "target" });
    res.send(out);
});

export default router