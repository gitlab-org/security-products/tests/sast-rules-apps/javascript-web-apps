import express from 'express'

const router = express.Router()

router.route("/").get((req, res) => {
    const links = [
        {
            url: "/xss/serialize/1?message=<script>alert('sd')</script>",
            text: "Xss-serialize-javascript unsafe",
        },
        {
            url: "/xss/serialize/safe/1?message=<script>alert('sd')</script>",
            text: "Xss-serialize-javascript safe",
        },
        {
            url: "/xss/serialize/safe/2?message=<script>alert('sd')</script>",
            text: "Xss-serialize-javascript safe",
        },
        {
            url: "/xss/squirrelly/1?message=<script>alert('sd')</script>",
            text: "Squirrelly unsafe",
        },
        {
            url: "/xss/squirrelly/safe/1?message=<script>alert('sd')</script>",
            text: "Squirrelly safe 1",
        },
        {
            url: "/xss/squirrelly/safe/2?message=<script>alert('sd')</script>",
            text: "Squirrelly safe 2"
        }, {
            url: "/xss/handlebars/noescape/1?message=<script>alert('XSS')</script>",
            text: "Xss-Handlebars-noescape unsafe",
        },
        {
            url: "/xss/handlebars/safe-string/1?message=<script>alert(%27xss%27)</script>",
            text: "Xss-handlebars-safe-string",
        },
        {
            url: "/xss/handlebars/noescape/safe/1?message=<script>alert('XSS')</script>",
            text: "Xss-Handlebars-noescape safe",
        },
        {
            url: "/xss/handlebars/noescape/safe/2?message=<script>alert('XSS')</script>",
            text: "Xss-Handlebars-noescape safe",
        },
        {
            url: "/xml/sax/1",
            text: "Sax test case scenario  1 - saxStream.ondoctype",
        },
        {
            url: "/xml/sax/2",
            text: "Sax test case scenario  2 - saxStream.on('doctype', function (doctype) {})",
        },
        {
            url: "/xml/sax/3",
            text: "Sax test case scenario  3 - require('sax').createStream(false)",
        },
        {
            url: "/jwt/none-algorithem/sign",
            text: "jsonwebtoken.sign with 'none' signing algorithm"
        },
        {
            url: "/jwt/none-algorithem/verify",
            text: "jsonwebtoken.verify with 'none' signing algorithm"
        },
        {
            url: "/jwt/none-algorithem/safe-sign-n-verify",
            text: "jsonwebtoken.sign and verify with 'HS256' signing algorithm"
        },
        {
            url: "/jwt/none-algorithem/unsafe-sign-n-verify",
            text: "jsonwebtoken.sign and verify with 'none' signing algorithm"
        },
        {
            url: "/jwt/none-algorithem/jose/sign-n-verify",
            text: "jose.sign and verify with 'HS256' signing algorithm"
        },
        {
          url: '/ssrf/wkhtmltoimage/1?url=http://your.collaborator.url',
          text: 'SSRF test case scenario  1'
        },
        {
          url: '/ssrf/wkhtmltoimage/2?url=http://your.collaborator.url',
          text: 'SSRF test case scenario  2'
        },
        {
          url: '/ssrf/wkhtmltoimage/3?url=https://www.youtube.com',
          text: 'SSRF test case scenario  3'
        },
        {
          url: 'ssrf/wkhtmltoimage/4?url=https://www.youtube.com',
          text: 'SSRF test case scenario  4'
        }
        // Add more links as needed
    ];

  // Start with an HTML structure
  let htmlResponse = `
            <html>
                <head>
                    <title>API Endpoints</title>
                    <style>
                        body { font-family: Arial, sans-serif; }
                        ul { list-style-type: none; }
                        li { margin: 10px 0; }
                        a { text-decoration: none; color: blue; }
                        a:hover { text-decoration: underline; }
                    </style>
                </head>
                <body>
                <div style="background-image: url(javascript:alert('XSS'))">
                    <h1>Available API Endpoints</h1>
                    <p> 
                        This application was built to demonstrate JS vulnerabilities 
                        identified by sast-rules.  if you need to add more test cases. 
                        create a route to handle the test cases and import that route into 
                        the list below and routes object in index.js
                    </p>
                    <ul>`

  // Add each link as a list item
  links.forEach(link => {
    htmlResponse += `<li><a href="${link.url}">${link.text}</a></li>`
  })

  // Close the HTML tags
  htmlResponse += `
                    </ul>
                    <br><br>
                </body>
            </html>`

  // Send the response
  res.send(htmlResponse)
})

export default router
