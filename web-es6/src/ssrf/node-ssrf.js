import got from 'got';
import express from "express";
import { request } from "urllib";
import bent from 'bent';

const getJSON = bent('json');
const getBuffer = bent('buffer');

const router = express.Router();

// http://localhost:3000/ssrf/node-ssrf/sample
router.route("/sample").get(async (req, res) => {
    res.send({
        res: "response from get"
    })
});

// http://localhost:3000/ssrf/node-ssrf/sample
router.route("/sample").post(async (req, res) => {
    res.send({
        res: "response from post"
    })
});

// http://localhost:3000/ssrf/node-ssrf/got/1?url=http://localhost:3000/ssrf/node-ssrf/sample
router.route("/got/1").get(async (req, res) => {
    // ruleid: rules_lgpl_javascript_ssrf_rule-node-ssrf
    const data = await got.post(req.query.url, {
        json: {}
    }).json();

    console.log(data);
    res.send("res from /got/1")
});

// http://localhost:3000/ssrf/node-ssrf/got/2?url=http://localhost:3000/ssrf/node-ssrf/sample
router.route("/got/2").get(async (req, res) => {
    const url = req.query.url;
    // ruleid: rules_lgpl_javascript_ssrf_rule-node-ssrf
    const data = await got.get(url).json();

    console.log(data);
    res.send("res from /got/1")
});

// http://localhost:3000/ssrf/node-ssrf/got/safe/1
router.route("/got/safe/1").get(async (req, res) => {
    // ok: rules_lgpl_javascript_ssrf_rule-node-ssrf
    const data = await got.post("http://localhost:3000/ssrf/node-ssrf/sample", {
        json: {}
    }).json();

    console.log(data);
    res.send("res from /got/1")
});

// http://localhost:3000/ssrf/node-ssrf/got/safe/2
router.route("/got/safe/2").get(async (req, res) => {
    var url = "http://localhost:3000/ssrf/node-ssrf/sample"
    // ok: rules_lgpl_javascript_ssrf_rule-node-ssrf
    const data = await got.post(url, {
        json: {}
    }).json();

    console.log(data);
    res.send("res from /got/1")
});

// http://localhost:3000/ssrf/node-ssrf/urllib/1?url=http://localhost:3000/ssrf/node-ssrf/sample
router.route("/urllib/1").get(async (req, res) => {
    // ruleid: rules_lgpl_javascript_ssrf_rule-node-ssrf
    const response = await request(req.query.url, {
        method: 'GET',
        dataType: 'json',
        data: {},
    });
    console.log(response.data)
    res.send({
        res: response.data
    })
});

// http://localhost:3000/ssrf/node-ssrf/bent/1?hostName=http://localhost:3000/&path=ssrf/node-ssrf/sample&jsonPath=http://localhost:3000/ssrf/node-ssrf/sample&bufferPath=https://gitlab.com/assets/illustrations/gitlab_logo-95e56730dc3513d2d29f49774e8a7c496bce38b491d1ce8c9e76b1e48128ccc4.svg
router.route("/bent/1").get(async (req, res) => {
    // ruleid: rules_lgpl_javascript_ssrf_rule-node-ssrf
    const get = bent(req.query.hostName, 'GET', 'json', 200);
    // ruleid: rules_lgpl_javascript_ssrf_rule-node-ssrf
    const response = await get(req.query.path, {});
    console.log(response)

     // ruleid: rules_lgpl_javascript_ssrf_rule-node-ssrf
     let obj = await getJSON(req.query.jsonPath)
     // ruleid: rules_lgpl_javascript_ssrf_rule-node-ssrf
     let buffer = await getBuffer(req.query.bufferPath)

     console.log(obj)
     console.log(buffer)

     res.send({
        response,
        obj,
        buffer
     })
});

export default router;