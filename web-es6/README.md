## web app es6

Testing JS code wrritten in ES6.

### Adding tests

Simply create a new route or find the applicable route under `src` to create the new tests.

- Create a sample route under the src/<vulnerability category> (ex: src/sample/sampleRoute.js )
- Implement the test cases that you need.
- Add the created route into the route object in index.js
- Add the route into the links list in home route to access it from the home route

### Running

Run:
```
cd web-es6
docker build -t web-app . && docker run -p 3000:3000 web-app
```

### Rules 

For Rule: https://gitlab.com/gitlab-org/security-products/sast-rules/-/blob/main/rules/lgpl/javascript/ssrf/rule-wkhtmltoimage_ssrf.yml

Replace the `/your/full/path/to/javascript-web-apps/web-es6` path with the path of your machine to `javascript-web-apps/web-es6` folder to get the `out.jpg` image created in your web-es6 folder.
```
docker build -t web-app . && docker run -v /your/full/path/to/javascript-web-apps/web-es6:/app -p 3000:3000 web-app
```
