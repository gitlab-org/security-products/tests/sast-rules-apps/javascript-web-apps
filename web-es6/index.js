import createAndStartServer from "./server.js"
import sampleRoute from "./src/sample/sampleRoute.js"
import xssRoute from "./src/xss/xss-serialize-javascript.js"
import xssHandlebarsNoescapeRoute from "./src/xss/xss-handlebars-noescape.js"
import xssHandlebarsRoute from "./src/xss/xss_handlebars_safe_string.js"
import homeRoute from "./src/HomeRoute.js"
import jwtHardcodedRoute from "./src/jwt/jwt-express-hardcoded.js"
import xssExpressRoute from "./src/xss/xss-express.js"
import squirrellyRoute from "./src/xss/xss-squirrelly-autoescape.js"
import jwtNoneAlgorithem from "./src/jwt/jwt_none_algorithm.js"
import hardcodedJwtSecretRoute from "./src/jwt/hardcodedJWTSecret.js"
import jwtExposedCredentialsRoute from "./src/jwt/jwt_exposed_credentials.js"
import saxRoute from "./src/xml/xxe-sax.js"
import nodeSsrfRoute from "./src/ssrf/node-ssrf.js"
import ssrf_wkhtmltoimage from './src/ssrf/ssrf_wkhtmltoimage.js'

const routers = {
    "/": homeRoute,
    "/sample": sampleRoute,
    "/xss/serialize": xssRoute,
    "/jwt/express-hardcoded": jwtHardcodedRoute,
    "/xss/squirrelly": squirrellyRoute,
    "/jwt/none-algorithem":jwtNoneAlgorithem,
    "/xss/express": xssExpressRoute,
    "/xss/handlebars/safe-string": xssHandlebarsRoute,
    "/xss/handlebars/noescape": xssHandlebarsNoescapeRoute,
    "/jwt/hardcoded-jwt-secret": hardcodedJwtSecretRoute,
    "/jwt/jwt_exposed_credentials": jwtExposedCredentialsRoute,
    "/xml/sax": saxRoute,
    "/ssrf/node-ssrf": nodeSsrfRoute,
    "/ssrf/wkhtmltoimage": ssrf_wkhtmltoimage
}

createAndStartServer(routers)
