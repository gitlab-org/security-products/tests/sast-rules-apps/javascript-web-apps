// License: GNU Lesser General Public License v3.0
// Rule ref: rules_lgpl_javascript_crypto_rule-node-insecure-random-generator

class TestInsecureRandomNumberGenerators {
  async run () {
    console.log('Running TestInsecureRandomNumberGenerators')
    testMathRandom()
    testPseudoRandomBytes()
    console.log('Stopping TestInsecureRandomNumberGenerators')
  }
}

function testMathRandom () {
  // ruleid:rules_lgpl_javascript_crypto_rule-node-insecure-random-generator
  const val = Math.random()
  console.log('Testing Math.random() : ' + val)
}

function testPseudoRandomBytes () {
  // ruleid:rules_lgpl_javascript_crypto_rule-node-insecure-random-generator
  const val1 = require('crypto').pseudoRandomBytes(16)
  console.log('Testing pseudoRandomBytes - part 1: ' + val1.toString('hex'))

  const crypto = require('crypto')

  // ruleid:rules_lgpl_javascript_crypto_rule-node-insecure-random-generator
  const val2 = crypto.pseudoRandomBytes(16)
  console.log('Testing pseudoRandomBytes - part 2: ' + val2.toString('hex'))

  // ruleid:rules_lgpl_javascript_crypto_rule-node-insecure-random-generator
  crypto.pseudoRandomBytes(16, (err, buffer) => {
    if (err) throw err
    console.log(`Testing pseudoRandomBytes - part 3: ${buffer.toString('hex')}`)
  })
}

module.exports = TestInsecureRandomNumberGenerators
