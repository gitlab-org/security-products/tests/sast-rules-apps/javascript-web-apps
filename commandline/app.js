const TestMD5Hash = require('./test-classes/TestMD5Hash.js')
const TestSHA1Hash = require('./test-classes/TestSHA1Hash.js')
const TestInsecureRandomNumberGenerators = require('./test-classes/TestInsecureRandomNumberGenerators.js')

// Add all your modules here
const classesToRun = {
  TestMD5Hash,
  TestSHA1Hash,
  TestInsecureRandomNumberGenerators
  // Add other classes with their corresponding keys
}

async function runClass (className) {
  try {
    if (classesToRun[className]) {
      const instance = new classesToRun[className]()
      await instance.run()
    } else {
      console.log(`Class ${className} not found.`)
    }
  } catch (error) {
    console.error(`Error running ${className}: ${error.message}`)
  }
}

function listClasses () {
  console.log('Available classes:')
  Object.keys(classesToRun).forEach(className => console.log(className))
}

async function processCommandLine (input) {
  switch (input.toLowerCase()) {
    case 'all':
      for (const className of Object.keys(classesToRun)) {
        await runClass(className)
      }
      break
    case 'list':
      listClasses()
      break
    default:
      await runClass(input)
  }
}

// Example command-line argument processing
const input = process.argv[2] || 'list' // Defaults to 'list' if no argument provided
processCommandLine(input)
