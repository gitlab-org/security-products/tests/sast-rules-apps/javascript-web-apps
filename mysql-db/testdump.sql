CREATE TABLE User(
	id INT PRIMARY KEY AUTO_INCREMENT,
	username VARCHAR(60),
	nickname VARCHAR(80),
	age INT
);

INSERT INTO User(username, nickname, age) VALUES("Phil Dunphy", "philly", 50);
INSERT INTO User(username, nickname, age) VALUES("Claire Dunphy", "Clairy", 45);
INSERT INTO User(username, nickname, age) VALUES("Luke Dunphy", "Lukey", 5);
INSERT INTO User(username, nickname, age) VALUES("Alex Dunphy", "Alexx", 15);
INSERT INTO User(username, nickname, age) VALUES("Hailey Dunphy", "Hail", 23);

ALTER USER 'root' IDENTIFIED WITH mysql_native_password BY 'password';

flush privileges;
