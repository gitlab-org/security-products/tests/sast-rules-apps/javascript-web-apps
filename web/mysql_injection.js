var mysql = require("mysql");

module.exports = function (app) {
  const connection = mysql.createPool({
    connectionLimit: 10,
    host: process.env.MYSQL_HOST || "localhost",
    user: process.env.MYSQL_USER || "root",
    password: process.env.MYSQL_PASSWORD || "password",
    database: process.env.MYSQL_DATABASE || "test",
  });


  // http://localhost:3000/mysql/one?username=Phil+Dunphy&nickname=philly
  app.get("/mysql/one", (req, res) => {
    // mysql
    // ruleid:rules_lgpl_javascript_database_rule-node-sqli-injection
    connection.query(
      "SELECT * FROM User WHERE username = '" +
        req.query.username +
        "' AND nickname = '" +
        req.query.nickname +
        "'",
      (err, rows) => {
        if (err) {
          res.json({
            success: false,
            err,
          });
        } else {
          res.json({
            success: true,
            rows,
          });
        }
      }
    );
  });

  // http://localhost:3000/mysql/two?id=1
  app.get("/mysql/two", (req, res) => {
    // mysql
    // ruleid:rules_lgpl_javascript_database_rule-node-sqli-injection
    var userId = req.query.id;
    var sql = "SELECT * FROM User WHERE id = " + userId;
    connection.query(sql, (err, rows) => {
      if (err) {
        res.json({
          success: false,
          err,
        });
      } else {
        res.json({
          success: true,
          rows,
        });
      }
    });
  });

  // http://localhost:3000/mysql/three?id=1
  app.get("/mysql/three", (req, res) => {
    // mysql
    // ruleid:rules_lgpl_javascript_database_rule-node-sqli-injection
    connection.query(
      "SELECT * FROM User WHERE id = " + req.query.id,
      (err, rows) => {
        if (err) {
          res.json({
            success: false,
            err,
          });
        } else {
          res.json({
            success: true,
            rows,
          });
        }
      }
    );
  });
};
