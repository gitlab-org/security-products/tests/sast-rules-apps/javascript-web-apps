const sax = require("sax")
const fs = require("fs")

module.exports = function (app) {
    // http://localhost:3000/xml/sax/1
    app.get("/xml/sax/1", async (req, res) => {
        var entityName, systemId

        var saxStream = sax.createStream(false)

        // ok: rules_lgpl_javascript_xml_rule-xxe-sax
        saxStream.on("error", function (e) {
            console.error("error!", e)
        })

        // ok: rules_lgpl_javascript_xml_rule-xxe-sax
        saxStream.onopentag = function (node) {
            console.log("opentag")
        }

        // ruleid: rules_lgpl_javascript_xml_rule-xxe-sax
        saxStream.ondoctype = async function (doctype) {
            const entityMatches = doctype.match(/<!ENTITY\s+(\w+)\s+SYSTEM\s+"([^"]+)">/);
            if (entityMatches) {
                entityName = entityMatches[1];
                systemId = entityMatches[2];
                console.log(`Found external entity: ${entityName} with SYSTEM "${systemId}"`);
            }
        }

        fs.createReadStream("assets/xml/xxe_xml.xml")
            .pipe(saxStream)
            .pipe(fs.createWriteStream("assets/xml/xxe_xml_cpy.xml"))

        res.send(`Check server console`);
    });

    // http://localhost:3000/xml/sax/2
    app.get("/xml/sax/2", async (req, res) => {
        var entityName, systemId

        var saxStream = sax.createStream(false)

        // ok: rules_lgpl_javascript_xml_rule-xxe-sax
        saxStream.on("error", function (e) {
            console.error("error!", e)
        })

        // ok: rules_lgpl_javascript_xml_rule-xxe-sax
        saxStream.on("opentag", function (node) {
            console.log("opentag")
        })

        // ruleid: rules_lgpl_javascript_xml_rule-xxe-sax
        saxStream.on("doctype", function (doctype) {
            const match = /<!ENTITY\s+(\w+)\s+SYSTEM\s+"([^"]+)"/.exec(doctype);
            if (match) {
                entityName = match[1];
                systemId = match[2];
                console.log(`Found external entity: ${entityName} with SYSTEM "${systemId}"`);
            }
        })

        fs.createReadStream("assets/xml/xxe_xml.xml")
            .pipe(saxStream)
            .pipe(fs.createWriteStream("assets/xml/xxe_xml_cpy.xml"))

        res.send(`Check server console`);
    });

    // http://localhost:3000/xml/sax/3
    app.get("/xml/sax/3", async (req, res) => {
        var entityName, systemId

        var saxStream = require("sax").createStream(false)

        // ok: rules_lgpl_javascript_xml_rule-xxe-sax
        saxStream.on("error", function (e) {
            console.error("error!", e)
        })

        // ok: rules_lgpl_javascript_xml_rule-xxe-sax
        saxStream.on("opentag", function (node) {
            console.log("opentag")
        })

        // ruleid: rules_lgpl_javascript_xml_rule-xxe-sax
        saxStream.on("doctype", function (doctype) {
            const match = /<!ENTITY\s+(\w+)\s+SYSTEM\s+"([^"]+)"/.exec(doctype);
            if (match) {
                entityName = match[1];
                systemId = match[2];
                console.log(`Found external entity: ${entityName} with SYSTEM "${systemId}"`);
            }
        })

        fs.createReadStream("assets/xml/xxe_xml.xml")
            .pipe(saxStream)
            .pipe(fs.createWriteStream("assets/xml/xxe_xml_cpy.xml"))

        res.send(`Check server console`);
    });
};
