window.testSetTimeout = function (name) {
  window.myname = name;

  // Exploit using http://localhost:3000/dangerous-sinks?name=hi%27);print();//
  // ruleid: javascript_eval_rule-eval-with-expression
  setTimeout(`console.log('${name}');`, 1000);

  // ok: javascript_eval_rule-eval-with-expression
  setTimeout("console.log(name);", 1000);

  // ok: javascript_eval_rule-eval-with-expression
  setTimeout(function () {
    console.log("setTimeout Safe Sink 2 : " + name);
  }, 500);

  const userScript = function () {
    console.log("setTimeout Safe Sink 3 : " + name);
  };

  // ok: javascript_eval_rule-eval-with-expression
  setTimeout(userScript, 500);

  // Exploit using http://localhost:3000/dangerous-sinks?name=hi%22);print();//
  // ruleid: javascript_eval_rule-eval-with-expression
  setTimeout("console.log('setTimeout Sink 5 : " + name + "');", 1000);

  window.x = 10;
  window.y = 22;
  // ok: javascript_eval_rule-eval-with-expression
  setTimeout("console.log('setTimeout Sink 6 :' + (x+y));", 1000);
};

window.testEval = function (name) {
  // ruleid: javascript_eval_rule-eval-with-expression
  eval(`console.log("${name}");`);

  // ruleid: javascript_eval_rule-eval-with-expression
  //  eval(name);
  //ruleid: javascript_eval_rule-eval-with-expression
  eval("console.log('" + name + "');");

  // Direct call to eval, uses local scope
  // ok: javascript_eval_rule-eval-with-expression
  eval("console.log(name);");

  // Indirect Call to eval, only accesses global vars
  // This looks like a harmless string but contains user input variable data that gets evaluated.
  //ruleid: javascript_eval_rule-eval-with-expression
  (0, eval)(`console.log("${name}");`);

  // call through alias
  //ruleid: javascript_eval_rule-eval-with-expression
  var foo = eval;
  foo(`console.log("${name}");`);

  //ruleid: javascript_eval_rule-eval-with-expression
  this.eval("console.log('" + name + "');");

  // Indirect call through member access
  const obj = { eval };
  //ruleid: javascript_eval_rule-eval-with-expression
  obj.eval("console.log('" + name + "');");

  // Indirect call through conditional operator
  //ruleid: javascript_eval_rule-eval-with-expression
  eval?.(`console.log("${name}");`);
};

window.testSetInterval = function (name) {
  window.myname = name;

  // Exploit using http://localhost:3000/dangerous-sinks?name=hi%27);print();//
  // ruleid: javascript_eval_rule-eval-with-expression
  window.intervalId1 = setInterval(`console.log('${name}');`, 1000);

  // ok: javascript_eval_rule-eval-with-expression
  window.intervalId2 = setInterval("console.log(myname);", 1000);

  // ok: javascript_eval_rule-eval-with-expression
  window.intervalId3 = setInterval(function () {
    console.log("setTimeout Safe Sink 2 : " + name);
  }, 500);

  // Passing user input to setTimeout
  const userScript = function () {
    console.log("setTimeout Safe Sink 3 : " + name);
  };

  // ok: javascript_eval_rule-eval-with-expression
  window.intervalId4 = setInterval(userScript, 500);

  // Exploit using http://localhost:3000/dangerous-sinks?name=hi%22);print();//
  // ruleid: javascript_eval_rule-eval-with-expression
  window.intervalId5 = setInterval(
    "console.log('setTimeout Sink 5 : " + name + "');",
    1000
  );

  window.x = 10;
  window.y = 22;
  // ok: javascript_eval_rule-eval-with-expression
  window.intervalId6 = setInterval(
    "console.log('setTimeout Sink 6 :' + (x+y));",
    1000
  );

  setTimeout(
    " clearInterval(intervalId1); " +
      "clearInterval(intervalId2);" +
      "clearInterval(intervalId3);" +
      "clearInterval(intervalId4);" +
      "clearInterval(intervalId5);" +
      "clearInterval(intervalId6);",
    1500
  );
};

window.testFunctionConstructor = function (name) {
  // To exploit - http://localhost:3000/dangerous-sinks?name=hi%22;alert(1);var%20a=%221
  // ruleid: javascript_eval_rule-eval-with-expression
  const sayHello = new Function(
    `return function (name) { var str = "Hello, ${name}"; return str; }`
  )();
  console.log(sayHello(name));

  // To exploit - http://localhost:3000/dangerous-sinks?name=%22%27;alert(1);//
  const userFormula = "var str = '" + name + "'; return str;";
  // ruleid: javascript_eval_rule-eval-with-expression
  const sum = new Function(userFormula)();
  console.log(sum);

  const userFormula2 =
    "return function (a) {var str = 'Hello, ' + a; return str;}";
  // ok: javascript_eval_rule-eval-with-expression
  const sum2 = new Function("a", userFormula2)();
  console.log(sum2(name));

  // ok: javascript_eval_rule-eval-with-expression
  const sayHello2 = new Function(
    "return function (name) { return `Hello, ${name}` }"
  )();
  // call the function
  console.log(sayHello2(name));

  // To exploit: http://localhost:3000/dangerous-sinks?name=hi%22);print();//
  const userData = `console.log("Function constructor call - ${name}");`;
  // ruleid: javascript_eval_rule-eval-with-expression
  console.log(Function(userData)());
};

window.onload = function () {
  var div = document.createElement("div");
  var p = document.createElement("p");
  p.innerHTML =
    "Check console logs.<br> Some exploit payloads:" +
    "<br>1. <a href='/dangerous-sinks?name=hi%27);print();//'>http://localhost:3000/dangerous-sinks?name=hi%27);print();//</a>" +
    "<br>2. <a href='/dangerous-sinks?name=hi%22;alert(1);var%20a=%221'>http://localhost:3000/dangerous-sinks?name=hi%22;alert(1);var%20a=%221</a>" +
    "<br>3. <a href='/dangerous-sinks?name=hi%22%27);print();//'>http://localhost:3000/dangerous-sinks?name=hi%22%27);print();//" +
    "<br>4. <a href='/dangerous-sinks?name=%22%27);alert(1);//'>http://localhost:3000/dangerous-sinks?name=%22%27;alert(1);//</a>";

  div.appendChild(p);
  document.body.appendChild(div);

  const urlParams = new URLSearchParams(window.location.search);

  // Get the value of the 'search' query parameter
  const name = urlParams.get("name");

  if (name) {
    testSetTimeout(name);
    testSetInterval(name);
    testFunctionConstructor(name);
    testEval(name);
  } else {
    window.alert(
      "Give a name in the url parameter -> http://localhost:3000/dangerous-sinks?name=Cameron"
    );
  }
};
