var Handlebars = require('handlebars');

module.exports = function (app) {

  // http://localhost:3000/xss/handlebars/noescape/1?message=<script>alert('XSS')</script>
  app.get("/xss/handlebars/noescape/1", async (req, res) => { 
    var template = "This is {{target}}";
    // ruleid: rules_lgpl_javascript_xss_rule-handlebars-noescape
    var out = Handlebars.compile(template, {noEscape: true})({target: req.query.message});
    res.send(out);
  });

   // http://localhost:3000/xss/handlebars/noescape/2?message=<script>alert('XSS')</script>
   app.get("/xss/handlebars/noescape/2", async (req, res) => { 
    var template = "This is {{target}}";
    // ruleid: rules_lgpl_javascript_xss_rule-handlebars-noescape
    var outFn = Handlebars.compile(template, {noEscape: true});
    var out = outFn({target: req.query.message});
    res.send(out);
  });

  // http://localhost:3000/xss/handlebars/noescape/safe/1?message=<script>alert('XSS')</script>
  app.get("/xss/handlebars/noescape/safe/1", async (req, res) => { 
    var template = "This is {{target}}";
    // ok: rules_lgpl_javascript_xss_rule-handlebars-noescape
    var out = Handlebars.compile(template)({target: req.query.message});
    res.send(out);
  });

  // http://localhost:3000/xss/handlebars/noescape/safe/2?message=<script>alert('sd')</script>
  app.get("/xss/handlebars/noescape/safe/2", async (req, res) => {
    var template = "This is {{target}}";
    // ok: rules_lgpl_javascript_xss_rule-handlebars-noescape
    var out = Handlebars.compile(template, {noEscape: false})({target: req.query.message});
    res.send(out);
  }); 

   // http://localhost:3000/xss/handlebars/noescape/safe/3?message=<script>alert('sd')</script>
   app.get("/xss/handlebars/noescape/safe/3", async (req, res) => {
    var template = "This is {{target}}";
    // ok: rules_lgpl_javascript_xss_rule-handlebars-noescape
    var out = Handlebars.compile(template, {noEscape: true})({target: "target"});
    res.send(out);
  });
}