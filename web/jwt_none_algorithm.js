const jwt = require("jsonwebtoken");
const jose = require("jose")
const { generateKeyPairSync } = require('crypto');

const { publicKey, privateKey } = generateKeyPairSync('rsa', {
    modulusLength: 2048,
    publicKeyEncoding: { type: 'spki', format: 'pem' },
    privateKeyEncoding: { type: 'pkcs8', format: 'pem' },
});

module.exports = function (app) {

    // http://localhost:3000/jwt-none-algorithm/jwt/
    /**
     * This endpoint is used to generate a JWT token using the jsonwebtoken.sign method.
     * It uses "none" as the signing algorithm. Due to this choice,
     * the JWT token will be generated without any signing.
    */
    app.get('/jwt-none-algorithm/jwt', (req, res) => {
        // ruleid: rules_lgpl_javascript_jwt_rule-node-jwt-none-algorithm
        var token = jwt.sign({ foo: 'bar' }, "", { algorithm: "none" });
        res.send(token)
    })

    // http://localhost:3000/jwt/jwt-none-algorithm/verify
    /**
     * This endpoint is used to validate JWT tokens using the jsonwebtoken.verify method.
     * It uses "none" as the signing algorithm. Due to this choice,
     * it will validate tokens without proper signing.
    */
    app.get('/jwt/jwt-none-algorithm/verify', (req, res) => {
        const token = req.query.token;
        const secretKey = '';

        // ruleid: rules_lgpl_javascript_jwt_rule-node-jwt-none-algorithm
        jwt.verify(token, secretKey, { algorithms: ['none'] }, (err, decoded) => {
            if (err) {
                console.error('Token verification failed:', err.message);
            } else {
                console.log('Token verified successfully:', decoded);
            }
        });

        res.send("Check server console")
    })

    // http://localhost:3000/jwt/jwt-none-algorithm/sign-n-verify/1
    /**
     * This endpoint is used to get the JWT token using the jsonwebtoken.sign method
     * and validate the token with jsonwebtoken.verify.
     * It uses none and RS256 as the verification algorithms.
     * Since none is used as a verification algorithm, it will validate tokens without proper signing.
    */
    app.get('/jwt/jwt-none-algorithm/sign-n-verify/1', (req, res) => {
        const payload = { user_id: 123, username: 'john_doe' };
        const secretKey = '';

        // ruleid: rules_lgpl_javascript_jwt_rule-node-jwt-none-algorithm
        const token = jwt.sign(payload, secretKey, { algorithm: 'none' });

        console.log('Generated Token:', token);


        // ruleid: rules_lgpl_javascript_jwt_rule-node-jwt-none-algorithm
        jwt.verify(token, secretKey, { algorithms: ['RS256', 'none'] }, (err, decoded) => {
            if (err) {
                console.error('Token verification failed:', err.message);
            } else {
                console.log('Token verified successfully:', decoded);
            }
        });

        res.send("Check server console")
    })

    // http://localhost:3000/jwt/jwt-none-algorithm/sign-n-verify/2
    /**
     * This endpoint is used to get the JWT token using the jsonwebtoken.sign method
     * and validate the token with jsonwebtoken.verify.
     * It uses RS256 as the verification and signing algorithm.
     * This endpoint will only create and validate tokens with proper signing.
    */
    app.get('/jwt/jwt-none-algorithm/sign-n-verify/2', (req, res) => {
        const payload = { user_id: 123, username: 'john_doe' };

        // ok: rules_lgpl_javascript_jwt_rule-node-jwt-none-algorithm
        const token = jwt.sign(payload, privateKey, { algorithm: 'RS256' });

        console.log('Generated Token:', token);

        // ok: rules_lgpl_javascript_jwt_rule-node-jwt-none-algorithm
        jwt.verify(token, publicKey, { algorithms: ['RS256'] }, (err, decoded) => {
            if (err) {
                console.error('Token verification failed:', err.message);
            } else {
                console.log('Token verified successfully:', decoded);
            }
        });

        res.send("Check server console")
    })

    // http://localhost:3000/jose/jwt-none-algorithm
    /**
     * This endpoint is used to get the JWT token using the jose.SignJWT method
     * and validate the token with jose.jwtVerify.
     * It uses HS256 as the verification and signing algorithm.
     * This function will only create and validate tokens with proper signing.
     */
    app.get('/jose/jwt-none-algorithm', async (req, res) => {
        const payload = { user_id: 123, username: 'john_doe' };
        const key = new TextEncoder().encode(
            'cc7e0d44fd473002f1c42167459001140ec6389b7353f8088f4d9a95f2f596f2',
        )

        // ok: rules_lgpl_javascript_jwt_rule-node-jwt-none-algorithm
        const token = await new jose.SignJWT(payload)
            .setProtectedHeader({ alg: 'HS256' })
            .setIssuedAt()
            .setIssuer('urn:example:issuer')
            .setAudience('urn:example:audience')
            .setExpirationTime('2h')
            .sign(key)

        console.log('Generated Token: ', token);

        // ok: rules_lgpl_javascript_jwt_rule-node-jwt-none-algorithm
        const tokenDecrypted = await jose.jwtVerify(token, key, { algorithms: ['HS256'] }, {
            issuer: 'urn:example:issuer',
            audience: 'urn:example:audience',
        });

        console.log({ tokenDecrypted })
        res.send("Token: " + tokenDecrypted)
    })
}
