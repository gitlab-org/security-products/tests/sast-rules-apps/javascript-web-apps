var { expressjwt } = require('express-jwt');
var { expressjwt: jwt } = require('express-jwt');
const jsonwebtoken = require("jsonwebtoken");

// NOTE: 
// Use http://localhost:3000/jwt/express-hardcoded/sign-in for token generation
// and use it in the rest of the test cases 
// SAMPLE CURL: 
// curl --location 'http://localhost:3000/jwt/express-hardcoded/safe-route' \
// --header 'Authorization: Bearer <TOKEN>'
module.exports = function (app) {

    // http://localhost:3000/jwt/express-hardcoded/sign-in
    app.get("/jwt/express-hardcoded/sign-in",
        async (req, res) => {
            const payload = { user_id: 123, username: 'john_doe' };

            const token = jsonwebtoken.sign(payload, "yourSecretKey", { algorithm: 'HS256', issuer: 'http://issuer' });
            console.log('Generated Token:', token);
            res.send(token)
        });

    // http://localhost:3000/jwt/express-hardcoded/auth-route-1
    app.get("/jwt/express-hardcoded/auth-route-1",
        // ruleid: rules_lgpl_javascript_jwt_rule-jwt-express-hardcoded
        expressjwt({ secret: "yourSecretKey", algorithms: ['HS256'] }),
        async (req, res) => {
            res.send("auth-route-auth-route-1 response");
        });

    // http://localhost:3000/jwt/express-hardcoded/auth-route-2
    const options = { secret: "yourSecretKey", algorithms: ['HS256'] }
    app.get("/jwt/express-hardcoded/auth-route-2",
        // ruleid: rules_lgpl_javascript_jwt_rule-jwt-express-hardcoded
        expressjwt(options),
        async (req, res) => {
            res.send("auth-route-auth-route-2 response");
        });

    // http://localhost:3000/jwt/express-hardcoded/free-route-1
    // ok:rules_lgpl_javascript_jwt_rule-jwt-express-hardcoded
    app.get("/jwt/express-hardcoded/free-route-1",
        async (req, res) => {
            res.send("auth-route-free-route-1 response");
        });

    // http://localhost:3000/jwt/express-hardcoded/auth-route-3    
    app.get('/jwt/express-hardcoded/auth-route-3',
        // ruleid:rules_lgpl_javascript_jwt_rule-jwt-express-hardcoded
        jwt({ secret: 'yourSecretKey', algorithms: ['HS256'] }), function (req, res) {
            res.send("auth-route-auth-route-3 response");
        });

    let hardcodedSecret = 'yourSecretKey'

    // http://localhost:3000/jwt/express-hardcoded/auth-route-4
    // ruleid:rules_lgpl_javascript_jwt_rule-jwt-express-hardcoded
    app.get('/jwt/express-hardcoded/auth-route-4', jwt({ secret: hardcodedSecret, algorithms: ['HS256'] }), function (req, res) {
        res.send("auth-route-auth-route-4 response");
    });

    let secret = "yourSecretKey"
    const opts = Object.assign({ issuer: 'http://issuer' }, { secret, algorithms: ['HS256'] })

    // http://localhost:3000/jwt/express-hardcoded/auth-route-5
    // ruleid:rules_lgpl_javascript_jwt_rule-jwt-express-hardcoded
    app.get('/jwt/express-hardcoded/auth-route-5', jwt(opts), function (req, res) {
        res.send("auth-route-auth-route-5 response");
    });

    let secret2 = { secret: "yourSecretKey" }
    const opts2 = Object.assign({ issuer: 'http://issuer' }, secret2, { algorithms: ['HS256'] })
    // http://localhost:3000/jwt/express-hardcoded/auth-route-6
    // ruleid:rules_lgpl_javascript_jwt_rule-jwt-express-hardcoded
    app.get('/jwt/express-hardcoded/auth-route-6', jwt(opts2), function (req, res) {
        res.send("auth-route-auth-route-6 response");
    });
}