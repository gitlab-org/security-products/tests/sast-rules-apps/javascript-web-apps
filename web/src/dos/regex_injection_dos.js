const RE2 = require("re2");

module.exports = function (app) {

    const longStr = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"

    // http://localhost:3000/dos/regex_injection_dos/RegExp/1
    /* 
    curl --location 'http://localhost:3000/dos/regex_injection_dos/RegExp/1?regex=(a%2B)%2Bb.' \
         --header 'Content-Type: application/json' \
         --data '{
             "regex": "(a+)+b."
         }'  
    */
    app.post('/dos/regex_injection_dos/RegExp/1', (req, res) => {
        // ruleid: javascript_dos_rule-non-literal-regexp
        var regex = new RegExp(req.body.regex )
        console.log("Start", performance.now())
        regex.test(longStr)
        console.log("End", performance.now())
        res.send("Check server console")
    })

    // http://localhost:3000/dos/regex_injection_dos/RegExp/2
    /* 
    curl --location 'http://localhost:3000/dos/regex_injection_dos/RegExp/1?regex=(a%2B)%2Bb.' \
         --header 'Content-Type: application/json' \
         --data '{
             "regex": "(a+)+b.",
             "options": "i"
         }'  
    */
    app.post('/dos/regex_injection_dos/RegExp/2', (req, res) => {
        // ruleid: javascript_dos_rule-non-literal-regexp
        var regex = new RegExp(req.body.regex, req.body.options)
        console.log("Start", performance.now())
        regex.test(longStr)
        console.log("End", performance.now())
        res.send("Check server console")
    })

    // http://localhost:3000/dos/regex_injection_dos/RegExp/safe/1
    /* 
    curl --location 'http://localhost:3000/dos/regex_injection_dos/RegExp/safe/1' \
         --header 'Content-Type: application/json' \
         --data '{
             "regex": "(a+).",
             "options": "i"
         }'  
    */
     app.post('/dos/regex_injection_dos/RegExp/safe/1', (req, res) => {
        //ok: javascript_dos_rule-non-literal-regexp
        var regex = new RegExp("b", req.body.options)
        console.log("Start", performance.now())
        regex.test(longStr)
        console.log("End", performance.now())
        res.send("Check server console")
    })

    // http://localhost:3000/dos/regex_injection_dos/split/1
    /*
    curl --location 'http://localhost:3000/dos/regex_injection_dos/split/1' \
    --header 'Content-Type: application/json' \
    --data '{
        "regex": "(a+)+b."
    }'
    */
    app.post('/dos/regex_injection_dos/split/1', (req, res) => {
        console.log("Start", performance.now())
        console.log(req.body.regex)
        const arr =  longStr.split(req.body.regex)
        console.log(arr)
        console.log("End", performance.now())
        res.send("Check server console")
    })

    // http://localhost:3000/dos/regex_injection_dos/split/2
    /*
    curl --location 'http://localhost:3000/dos/regex_injection_dos/split/2' \
    --header 'Content-Type: application/json' \
    --data '{
        "regex": "(a+)+b."
    }'
    */
    app.post('/dos/regex_injection_dos/split/2', (req, res) => {
        console.log("Start", performance.now())
        console.log(req.query)
        // ruleid:  javascript_dos_rule-non-literal-regexp
        var regex = new RegExp(req.body.regex)
        const arr =  longStr.split(regex)
        console.log(arr)
        console.log("End", performance.now())
        res.send("Check server console")
    })

    // http://localhost:3000/dos/regex_injection_dos/match/1
    /*
    curl --location 'http://localhost:3000/dos/regex_injection_dos/match/1' \
    --header 'Content-Type: application/json' \
    --data '{
        "regex": "(a+)+b."
    }'
    */
    app.post('/dos/regex_injection_dos/match/1', (req, res) => {
        console.log("Start", performance.now())
        // ruleid: javascript_dos_rule-non-literal-regexp
        const arr =  longStr.match(req.body.regex)
        console.log(arr)
        console.log("End", performance.now())
        res.send("Check server console")
    })

    // http://localhost:3000/dos/regex_injection_dos/match/safe/1
    /*
    curl --location 'http://localhost:3000/dos/regex_injection_dos/match/safe/1' \
    --header 'Content-Type: application/json' \
    --data '{
        "regex": "(a+)+b."
    }'
    */
    app.post('/dos/regex_injection_dos/match/safe/1', (req, res) => {
        console.log("Start", performance.now())
        // ok: javascript_dos_rule-non-literal-regexp
        const arr =  longStr.match(/a/)
        console.log(arr)
        console.log("End", performance.now())
        res.send("Check server console")
    })

    // http://localhost:3000/dos/regex_injection_dos/search/1
    /*
    curl --location 'http://localhost:3000/dos/regex_injection_dos/search/1' \
    --header 'Content-Type: application/json' \
    --data '{
        "regex": "(a+)+b."
    }'
    */
    app.post('/dos/regex_injection_dos/search/1', (req, res) => {
        console.log("Start", performance.now())
        // ruleid: javascript_dos_rule-non-literal-regexp
        const arr =  longStr.search(req.body.regex)
        console.log(arr)
        console.log("End", performance.now())
        res.send("Check server console")
    })

    // http://localhost:3000/dos/regex_injection_dos/search/safe/1
    app.get('/dos/regex_injection_dos/search/safe/1', (req, res) => {
        console.log("Start", performance.now())
        // ok: javascript_dos_rule-non-literal-regexp
        const arr =  longStr.search(/a/)
        console.log("End", performance.now())
        res.send("Check server console")
    })

    // http://localhost:3000/dos/regex_injection_dos/re2/safe/1
    /*
    curl --location 'http://localhost:3000/dos/regex_injection_dos/re2/safe/1' \
    --header 'Content-Type: application/json' \
    --data '{
        "regex": "(a+)+b.",
        "options": "i"
    }'
    */
    app.post('/dos/regex_injection_dos/re2/safe/1', (req, res) => {
        console.log("Start", performance.now())
        // ok: javascript_dos_rule-non-literal-regexp
        var re = new RE2(req.body.regex)
        re.exec(longStr)
        console.log("End", performance.now())
        res.send("Check server console")
    })
}