const express = require('express')
const dotEnv = require('dotenv')
const path = require('path')
const app = express()
const port = 3000
dotEnv.config()

app.use(express.urlencoded({ extended: true }))
app.use(express.json())

require('./mysql_injection')(app)
require('./pg_injection')(app)
require('./sequelize_injection')(app)
require('./eval_injection')(app)
require('./open_redirect2')(app)
require('./open_redirect1')(app)
require('./xss-serialize-javascript')(app)
require('./xss-handlebars-noescape')(app)
require('./src/xss/xss-express')(app)
require('./src/xss/xss_handlebars_safe_string')(app)
require('./xss-squirrelly-autoescape')(app)
require('./xml-xpath-injection')(app);
require('./xxe-sax')(app)
require('./mustache_escape')(app)
require('./child-process')(app)
require('./eval-require')(app)
require('./src/ssrf/ssrf_wkhtmltoimage')(app)
require('./xml-xpath-injection')(app);
require('./jwt_none_algorithm')(app);
require('./src/jwt/hardcodedJWTSecret')(app)
require('./src/jwt/jwt_exposed_credentials')(app)
require('./src/jwt/jwt-express-hardcoded')(app)
require('./src/traversal/express-lfr')(app)
require('./src/dos/regex_injection_dos')(app);
require('./src/ssrf/node-ssrf')(app);
require('./src/traversal/generic-path-traversal')(app)

// Serve static files from the 'public' directory
app.use(express.static(path.join(__dirname, 'public')))

app.set('view engine', 'hbs');

const server = app.listen(port, () => {
  console.log(`Express app listening at http://localhost:${port}`)
})

// app.get("/", (req, res) => {
//   res.send("Hello World!");
// });

app.get('/', (req, res) => {
  const links = [
    {
      url: '/mysql/one?username=Phil+Dunphy&nickname=philly',
      text: 'MySQL test 1'
    },
    { url: '/mysql/two?id=4', text: 'MySQL test 2' },
    {
      url: '/mysql/three?id=5',
      text: 'MySQL test 3'
    },
    { url: '/pg/one?id=4', text: 'Postgres test 1' },
    {
      url: '/pg/two?nickname=Lukey',
      text: 'Postgres test 2'
    },
    { url: '/seq/one?id=2', text: 'Sequelize test 1' },
    {
      url: '/seq/two?username=Phil+Dunphy&nickname=philly',
      text: 'Sequelize test 2'
    },
    {
      url: '/seq/three?username=Phil+Dunphy&nickname=philly',
      text: 'Sequelize test 3'
    },
    {
      url: '/seq/four?username=Phil+Dunphy&nickname=philly',
      text: 'Sequelize test 4'
    },
    { url: '/seq/five', text: 'Sequelize test 5' },
    {
      url: '/seq/six?username=Phil+Dunphy&nickname=philly',
      text: 'Sequelize test 6'
    },
    {
      url: '/seq/seven?username=Phil+Dunphy&nickname=philly',
      text: 'Sequelize test 7'
    },
    {
      url: '/seq/eight',
      text: 'Sequelize test 8'
    },
    {
      url: "/xss/serialize/1?message=<script>alert('sd')</script>",
      text: 'Xss-serialize-javascript unsafe'
    },
    {
      url: "/xss/serialize/safe/1?message=<script>alert('sd')</script>",
      text: 'Xss-serialize-javascript safe'
    },
    {
      url: "/xss/serialize/safe/2?message=<script>alert('sd')</script>",
      text: 'Xss-serialize-javascript safe'
    },
    {
      url: '/xss/squirrelly/1',
      text: 'Squirrelly safe'
    },
    {
      url: '/xss/squirrelly/safe/1',
      text: 'Squirrelly safe'
    },
    {
      url: '/xss/squirrelly/safe/2',
      text: 'Squirrelly safe'
    },
    {
      url: '/xss/handlebars/safe-string/1?message=<script>alert(%27xss%27)</script>',
      text: 'Xss-handlebars-safe-string'
    },
    {
      url: "/xss/handlebars/noescape/1?message=<script>alert('XSS')</script>",
      text: 'Xss-Handlebars-noescape unsafe'
    },
    {
      url: "/xss/handlebars/noescape/safe/1?message=<script>alert('XSS')</script>",
      text: 'Xss-Handlebars-noescape safe'
    },
    {
      url: "/xss/handlebars/noescape/safe/2?message=<script>alert('XSS')</script>",
      text: 'Xss-Handlebars-noescape safe'
    },
    {
      url: '/xml/sax/1',
      text: 'Sax test case scenario  1 - saxStream.ondoctype'
    },
    {
      url: '/xml/sax/2',
      text: "Sax test case scenario  2 - saxStream.on('doctype', function (doctype) {})"
    },
    {
      url: '/xml/sax/3',
      text: "Sax test case scenario  3 - require('sax').createStream(false)"
    },
    {
      url: '/ssrf/1?url=http://your.collaborator.url',
      text: 'SSRF test case scenario  1'
    },
    {
      url: '/ssrf/2?url=http://your.collaborator.url',
      text: 'SSRF test case scenario  2'
    },
    {
      url: '/ssrf/3?url=https://www.youtube.com',
      text: 'SSRF test case scenario  3'
    },
    {
      url: '/ssrf/4?url=https://www.youtube.com',
      text: 'SSRF test case scenario  4'
    },
    {
      url: '/ssrf/5?url=https://www.youtube.com',
      text: 'SSRF test case scenario  5'
    }, 
    {
      url: "/jwt-none-algorithm/jwt",
      text: "jsonwebtoken.sign with 'none' signing algorithm"
    },
    {
      url: "/jwt/jwt-none-algorithm/verify",
      text: "jsonwebtoken.verify with 'none' signing algorithm"
    },
    {
      url: "/jwt/jwt-none-algorithm/sign-n-verify/1",
      text: "jsonwebtoken.sign and verify with 'none' signing algorithm"
    },
    {
      url: "/jwt/jwt-none-algorithm/sign-n-verify/2",
      text: "jsonwebtoken.sign and verify with 'RS256' signing algorithm"
    },
    {
      url: "/jose/jwt-none-algorithm",
      text: "jose.sign and verify with 'HS256' signing algorithm"
    }
    // Add more links as needed
  ]

  // Start with an HTML structure
  let htmlResponse = `
        <html>
            <head>
                <title>API Endpoints</title>
                <style>
                    body { font-family: Arial, sans-serif; }
                    ul { list-style-type: none; }
                    li { margin: 10px 0; }
                    a { text-decoration: none; color: blue; }
                    a:hover { text-decoration: underline; }
                </style>
            </head>
            <body>
            <div style="background-image: url(javascript:alert('XSS'))">
                <h1>Available API Endpoints</h1>
                <p> 
                Feel free to change values of query parameters.
                Check the Readme file to see the db entries. 
                P.S. The request queries might look similar, but the db queries behind are structured in different formats to test for varied patterns.
                </p>
                <ul>`

  // Add each link as a list item
  links.forEach(link => {
    htmlResponse += `<li><a href="${link.url}">${link.text}</a></li>`
  })

  // Close the HTML tags
  htmlResponse += `
                </ul>
                <h1>Tests for JavaScript Mustache Escape</h1>
                <p> 
                Please visit "http://localhost:3000/mustache-escape" for the tests or click the button below.
                </p>
                <button onclick="goToMustacheEscape()"> 
                    Go to Mustache Escape 
                </button> 
              
                <script> 
                    function goToMustacheEscape() { 
                        window.location.href =  
                            "http://localhost:3000/mustache-escape"; 
                    } 
                </script> 

                <br><br>
                <p> For Rule : 'https://gitlab.com/gitlab-org/security-products/sast-rules/-/blob/main/javascript/eval/rule-eval-with-expression.yml'
                    -> Navigate to <a href="/dangerous-sinks?name=Cameron">http://localhost:3000/dangerous-sinks?name=Cameron</a> </p>
                <p> For Rule : 'https://gitlab.com/gitlab-org/security-products/sast-rules/-/blob/main/rules/lgpl/javascript/redirect/rule-express_open_redirect.yml'
                    -> Navigate to <a href="/redirect">http://localhost:3000/redirect</a> </p>                
                <p> For Rule : 'https://gitlab.com/gitlab-org/security-products/sast-rules/-/blob/main/rules/lgpl/javascript/redirect/rule-express_open_redirect2.yml'
                    -> Navigate to <a href='/redirect-two'>http://localhost:3000/redirect-two</a></p>

            </body>
        </html>`

  // Send the response
  res.send(htmlResponse)
})

const socketIo = require('socket.io');
const io = socketIo(server);

// Handle socket connections
io.on('connection', (socket) => {
  console.log('A user connected');
  
  // Handle disconnection
  socket.on('disconnect', () => {
      console.log('User disconnected');
  });
});

// Log server errors
server.on('error', (error) => {
  console.error('Server error:', error);
});