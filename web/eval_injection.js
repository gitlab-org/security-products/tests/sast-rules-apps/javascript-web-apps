var mysql = require("mysql");

module.exports = function (app) {
  // http://localhost:3000/dangerous-sinks?name=Cameron
  app.get("/dangerous-sinks", (req, res) => {
    res.send("<script src='/script.js'></script>");
  });
};
