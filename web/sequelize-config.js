const Sequelize = require("sequelize");

// PostgreSQL configuration
const pgSequelize = new Sequelize("test", "postgres", "password", {
  host: "pg-db",
  dialect: "postgres",
});

// MySQL configuration
const mysqlSequelize = new Sequelize("test", "root", "password", {
  host: "mysql-db",
  dialect: "mysql",
});

module.exports = { pgSequelize, mysqlSequelize };
