const { Pool } = require("pg");

module.exports = function (app) {
  const pool = new Pool({
    user: process.env.POSTGRES_USER || "postgres",
    host: process.env.POSTGRES_HOST || "localhost",
    database: process.env.POSTGRES_DATABASE || "test",
    password: process.env.POSTGRES_PASSWORD || "password",
    port: process.env.POSTGRES_PORT || 5432,
  });

  //http://localhost:3000/pg/one?id=1
  app.get("/pg/one", async (req, res) => {
    try {
      // ruleid:rules_lgpl_javascript_database_rule-node-sqli-injection
      var inp = req.query.id;
      const result = await pool.query("SELECT * FROM myuser WHERE id = " + inp);

      res.json({
        success: true,
        rows: result.rows,
      });
    } catch (err) {
      res.json({
        success: false,
        err,
      });
    }
  });

  //http://localhost:3000/pg/two?nickname=Lukey
  app.get("/pg/two", async (req, res) => {
    try {
      // ruleid:rules_lgpl_javascript_database_rule-node-sqli-injection
      const query =
        "SELECT * FROM myuser WHERE nickname='" + req.query.nickname + "'";

      const result = await pool.query(query);

      res.json({
        success: true,
        rows: result.rows,
      });
    } catch (err) {
      res.json({
        success: false,
        err,
      });
    }
  });
};
