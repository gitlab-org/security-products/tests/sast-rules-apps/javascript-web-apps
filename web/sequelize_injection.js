const { pgSequelize } = require("./sequelize-config");
const { Sequelize } = require("sequelize");

var username;
var nickname;

module.exports = function (app) {
  //http://localhost:3000/seq/one?id=1
  app.get("/seq/one", async (req, res) => {
    // ruleid:rules_lgpl_javascript_database_rule-node-sqli-injection
    var query = "SELECT * FROM myuser WHERE id = '" + req.query.id + "'";
    pgSequelize
      .query(query, { type: pgSequelize.QueryTypes.SELECT })
      .then((users) => {
        console.log(users);
        res.json({
          success: true,
          rows: users,
        });
      })
      .catch((error) => {
        console.error(error);
        res.json({
          success: false,
          error,
        });
      });
  });

  //http://localhost:3000/seq/two?username=Phil+Dunphy&nickname=philly
  app.get("/seq/two", async (req, res) => {
    // ruleid:rules_lgpl_javascript_database_rule-node-sqli-injection
    username = req.query.username;
    nickname = req.query.nickname;
    query = `SELECT * FROM myuser WHERE username = '${username}' AND nickname = '${nickname}'`;
    pgSequelize
      .query(query, { type: pgSequelize.QueryTypes.SELECT })
      .then((users) => {
        console.log(users);
        res.json({
          success: true,
          rows: users,
        });
      })
      .catch((error) => {
        console.error(error);
        res.json({
          success: false,
          error,
        });
      });
  });

  //http://localhost:3000/seq/three?username=Phil+Dunphy&nickname=philly
  app.get("/seq/three", async (req, res) => {
    username = req.query.username;
    nickname = req.query.nickname;

    extraCaseThree(username, nickname, res);
  });

  function extraCaseThree(username, nickname, res) {
    // ruleid: rules_lgpl_javascript_database_rule-node-sqli-injection
    pgSequelize
      .query(
        `SELECT * FROM myuser WHERE username = '${username}' AND nickname = '${nickname}'`,
        { type: pgSequelize.QueryTypes.SELECT }
      )
      .then((users) => {
        console.log(users);
        res.json({
          success: true,
          rows: users,
        });
      })
      .catch((error) => {
        console.error(error);
        res.json({
          success: false,
          error,
        });
      });
  }

  //http://localhost:3000/seq/four?username=Phil+Dunphy&nickname=philly
  app.get("/seq/four", async (req, res) => {
    username = req.query.username;
    nickname = req.query.nickname;

    extraCaseFour(username, nickname, res);
  });

  function extraCaseFour(username, nickname, res) {
    // ruleid: rules_lgpl_javascript_database_rule-node-sqli-injection
    var query = `SELECT * FROM myuser WHERE username = '${username}' AND nickname = '${nickname}'`;
    pgSequelize
      .query(query, { type: pgSequelize.QueryTypes.SELECT })
      .then((users) => {
        console.log(users);
        res.json({
          success: true,
          rows: users,
        });
      })
      .catch((error) => {
        console.error(error);
        res.json({
          success: false,
          error,
        });
      });
  }

  //http://localhost:3000/seq/five
  app.get("/seq/five", async (req, res) => {
    username = req.query.username;
    nickname = req.query.nickname;

    extraCaseFive(res);
  });

  function extraCaseFive(res) {
    const abc = "Claire Dunphy";
    const def = "Clairy";
    // ok: rules_lgpl_javascript_database_rule-node-sqli-injection
    var query = `SELECT * FROM myuser WHERE username = '${abc}' AND nickname = '${def}'`;
    pgSequelize
      .query(query, { type: pgSequelize.QueryTypes.SELECT })
      .then((users) => {
        console.log(users);
        res.json({
          success: true,
          rows: users,
        });
      })
      .catch((error) => {
        console.error(error);
        res.json({
          success: false,
          error,
        });
      });
  }

  //http://localhost:3000/seq/six?username=Phil+Dunphy&nickname=philly
  app.get("/seq/six", async (req, res) => {
    username = req.query.username;
    nickname = req.query.nickname;

    extraCaseSix(username, nickname, res);
  });

  function extraCaseSix(username, nickname, res) {
    // ruleid: rules_lgpl_javascript_database_rule-node-sqli-injection
    pgSequelize
      .query(
        "Select * from myuser where username = '" +
          username +
          "' and nickname = '" +
          nickname +
          "'",
        { type: pgSequelize.QueryTypes.SELECT }
      )
      .then((users) => {
        console.log(users);
        res.json({
          success: true,
          rows: users,
        });
      })
      .catch((error) => {
        console.error(error);
        res.json({
          success: false,
          error,
        });
      });
  }

  //http://localhost:3000/seq/seven?username=Phil+Dunphy&nickname=philly
  app.get("/seq/seven", async (req, res) => {
    username = req.query.username;
    nickname = req.query.nickname;

    extraCaseSeven(username, nickname, res);
  });

  function extraCaseSeven(username, nickname, res) {
    // ruleid: rules_lgpl_javascript_database_rule-node-sqli-injection
    var query =
      "Select * from myuser where username = '" +
      username +
      "' and nickname ='" +
      nickname +
      "'";
    pgSequelize
      .query(query, { type: pgSequelize.QueryTypes.SELECT })
      .then((users) => {
        console.log(users);
        res.json({
          success: true,
          rows: users,
        });
      })
      .catch((error) => {
        console.error(error);
        res.json({
          success: false,
          error,
        });
      });
  }

  //http://localhost:3000/seq/eight
  app.get("/seq/eight", async (req, res) => {
    username = req.query.username;
    nickname = req.query.nickname;

    extraCaseEight(res);
  });

  function extraCaseEight(res) {
    // ok: rules_lgpl_javascript_database_rule-node-sqli-injection
    pgSequelize
      .query(
        "Select * from myuser where username = '" +
          "Luke Dunphy" +
          "' and nickname = '" +
          "Lukey" +
          "'",
        { type: pgSequelize.QueryTypes.SELECT }
      )
      .then((users) => {
        console.log(users);
        res.json({
          success: true,
          rows: users,
        });
      })
      .catch((error) => {
        console.error(error);
        res.json({
          success: false,
          error,
        });
      });
  }
};
