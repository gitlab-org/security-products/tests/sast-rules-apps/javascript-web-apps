// For rule: javascript_crypto_rule - NodeLibcurlSSLVerificationDisable

const https = require('https')
const fs = require('fs')

const options = {
  key: fs.readFileSync('private.key'),
  cert: fs.readFileSync('certificate.crt')
}

https
  .createServer(options, (req, res) => {
    res.writeHead(200)
    res.end('Hello, world!\n')
  })
  .listen(8443, () => {
    console.log('Server running at https://localhost:8443/')
  })
