## Testing node-libcurl SSL rule MRE

For Rule : javascript_crypto_rule-NodeLibcurlSSLVerificationDisable

### Running

To run MRE for NodeLibcurlSSLVerificationDisable Rule
```
docker-compose -f docker-compose.node-libcurl.yml up
```
Stop all the running containers for NodeLibcurlSSLVerificationDisable MRE
```
docker-compose -f docker-compose.node-libcurl.yml down
```
(Use this if you make code changes (for instance if you switch on verbose mode))
Stop and remove all containers, networks, images used by services in docker-compose.yml file:
```
docker-compose -f docker-compose.node-libcurl.yml down --rmi all
```

## Testing Node TLS Reject rule MRE

For Rule : rules_lgpl_javascript_crypto_rule-node-tls-reject

### Running

To run MRE for Node TLS Reject Rule
```
docker-compose -f docker-compose.tls-reject.yml up
```
Stop all the running containers for Node TLS Reject Rule MRE
```
docker compose -f docker-compose.tls-reject.yml down
```
(Use this if you make code changes (for instance if you switch on verbose mode))
Stop and remove all containers, networks, images used by services in docker-compose.yml file:
```
docker compose -f docker-compose.tls-reject.yml down --rmi all
```